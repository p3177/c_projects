 #ifndef LINKEDLISTH
#define LINKEDLISTH

#define FALSE 0
#define TRUE !FALSE
#define _CRT_SECURE_NO_WARNINGS
#define MAX_LEN 300


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>


// Frame struct
typedef struct Frame
{
	char*		name;
	unsigned int	duration;
	char*		path;  
} Frame;


// Link (node) struct
typedef struct FrameNode
{
	Frame* frame;
	struct FrameNode* next;
} FrameNode;

bool getFrameInfo(int* frameTime, char* framePath, char* frameName, FrameNode* first);
FrameNode* createFrame(char framePath[], char frameName[], int frameTime);
void addToList(FrameNode** first, FrameNode* person);
void printElementList(FrameNode* first);
bool removeFromList(FrameNode** first, char name[]);
void changeAllDurations(FrameNode** first, int newDuration);
void myFgets(char str[], int n);
bool changeDuration(FrameNode** first, char* frameName);
int listLength(FrameNode* curr);
void changeIndex(FrameNode** first);
bool checkValueFrame(FrameNode* head, char* name);
void freelist(FrameNode** first);
FrameNode* popFrame(FrameNode** head, char* name);
void printElementList(FrameNode* first);
void saveProject(FrameNode* first);
bool openProject(FrameNode** first);
void reverse(FrameNode** first);

#endif
