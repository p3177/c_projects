/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:                          *
* Credits:                       *
**********************************/

#define CV_IGNORE_DEBUG_BUILD_GUARD
//#define _CRTDBG_MAP_ALLOC
#define _CRT_SECURE_NO_WARNINGS
#define MAX_LEN 300
#include <opencv2/imgcodecs/imgcodecs_c.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui_c.h>
#include <crtdbg.h>
#include <stdio.h>
#include "view.h"
#include "linkedList.h"


int greeting(void);

int main(void)
{

	int choice = 0;
	int mode = 0;
	char framePath[MAX_LEN];
	char frameName[MAX_LEN];
	char FrameName[MAX_LEN];
	int newDuration = 0;
	int frameTime = 0;
	FrameNode* FrameList = NULL;
	FrameNode* temp = NULL;

//asks user to choose mode.	
	do
	{
		printf("Welcome to Magshimim Movie Maker! What would you like to do?\n");
		printf(" [0] Create a new project\n");
		printf(" [1] Load existing project\n");
		scanf("%d", &mode);
		getchar();
	} while ((mode < 0) || (mode > 1));

//opens project if chosen.
	if (mode)
	{
		openProject(&FrameList);
	}

//switch case to edit video.
	do
	{
		choice = greeting();
		switch (choice)
		{
		case 0:
			printf("Thanks for using.\n");
			break;
		case 1:
			printf("*** Creating new frame ***\n");
			if (getFrameInfo(&frameTime, framePath, frameName, FrameList))
			{
				temp = createFrame(framePath, frameName, frameTime);
				addToList(&FrameList, temp);
			}
			break;
		case 2:
			printf("Enter name to remove:\n");
			myFgets(frameName, MAX_LEN);
			if (removeFromList(&FrameList, frameName))
			{
				printf("%s was removed from video!\n\n", frameName);
			}
			else
			{
				printf("%s is not in video!\n\n", frameName);
			}
			break;
		case 3:
			changeIndex(&FrameList);
			break;
		case 4:
			printf("Enter name to change duration for:\n");
			myFgets(frameName, MAX_LEN);
			if (changeDuration(&FrameList, frameName))
			{
				printf("Frame duration was changed.\n");
			}
			else
			{
				printf("there's no frame with that name.\n\n");
			}
			break;
		case 5:
			printf("Enter duration to change:\n");
			scanf("%d", &newDuration);
			getchar();
			changeAllDurations(&FrameList, newDuration);
			printf("All frame durations were changed.\n\n");
			break;
		case 6:
			printElementList(FrameList);
			break;
		case 7:
			play(FrameList);
			break;
		case 8:
			reverse(&FrameList);
			play(FrameList);
			reverse(&FrameList);
			break;
		case 9:
			saveProject(FrameList);
			break;
		default:
			printf("NOT A VALID OPTION, PLEASE CHOOSE A DIFFRENT ONE.\n\n");
			break;
		}

	} while (choice != 0);
	if (FrameList)
	{
		freelist(&FrameList);
	}
}


/*
pressents user with options and gets choice.
Input: none.
Output : users choice.
*/
int greeting(void)
{
	printf("What would you like to do?\n");
	printf(" [0] Exit\n");
	printf(" [1] Add new Frame\n");
	printf(" [2] Remove a Frame\n");
	printf(" [3] Change Frame index\n");
	printf(" [4] Change Frame duration\n");
	printf(" [5] Change duration of all frames\n");
	printf(" [6] List frames\n");
	printf(" [7] Play movie!\n");
	printf(" [8] Play movie rewinded\n");
	printf(" [9] save project\n");
	int choice = 0;
	scanf_s("%d", &choice);
	getchar();
	return choice;
}




