/*********************************
* Class: MAGSHIMIM Final Project *
* Likned List.				 	 *
**********************************/

#include "linkedList.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_LEN 300
#define _CRT_SECURE_NO_WARNINGS
#define DELIMETER "#"


/*
Gets required information from user to set up a new frame, 
checks if name already exists, in case it is, yeets user back 
to the menu, does the same if file path is not valid.
Input: FrameNode* first, char* name.
Output: true/false.
*/
bool getFrameInfo(int* frameTime, char* framePath, char* frameName, FrameNode* first)
{
	FrameNode* curr = first;
	
	printf("Please insert frame path:\n");
	myFgets(framePath, MAX_LEN);
	FILE* handler = fopen(framePath, "rb");
	if (!handler)
	{
		printf("path doesnt lead to a frame..Please check your path.\n");
		return false;
	}
	printf("Please insert frame duration(in miliseconds):\n");
	scanf("%d", frameTime);
	getchar();
	printf("Please choose a name for that frame:\n");
	myFgets(frameName, MAX_LEN);
	while (curr)
	{
		if (!strcmp(curr->frame->name, frameName))
		{
			printf("Name is already in usage for a frame.\n");
			return false;
		}
		curr = curr->next;
	} 
	
	fclose(handler);
	return true;
}

/*
checks if frame actually exist in list.
Input: FrameNode* first, char* name.
Output: true/false.
*/
bool checkValueFrame(FrameNode* first, char* name)
{
	bool check = false;
	FrameNode* curr = first;
	while (curr)
	{
		if (!strcmp(curr->frame->name, name))
		{
			check = true;
			break;
		}
		curr = curr->next;
	}
	return check;
}


/*
Changes index of a linked list of FrameNode struct according to user
input, also gets info for action from user.
Input: FrameNode** first
Output: None.
*/
void changeIndex(FrameNode** first)
{
	int index = 0;
	char name[MAX_LEN];
	FrameNode* frame = NULL;
	FrameNode* curr = NULL;
	//when user enters a name that doesn't match, it would ask to enter again.
	do
	{
		printf("Enter the name of the frame:\n");
		myFgets(name, MAX_LEN);
	} 
	while (!checkValueFrame(*first, name));

	frame = popFrame(first, name);
	curr = *first;
	
	//when user enters an invalid input, it would ask to enter again.
	do
	{
		printf("Enter the index in the movie you wish to place the frame:\n");
		scanf("%d", &index);
	} 
	while ((index >= listLength(curr)) || (index <= 0));
	
	
	while (curr && index != 2)//if there's still indexes to be reduced +  
	{						 // any value in current after first relocation.	
		if (index == 1)    
		{
			frame->next = curr;
			*first = frame;
		}
		curr = curr->next;
		index--;
	}
	
	if (curr)//edge one: first index
	{
		frame->next = curr->next;
		curr->next = frame;
	}
}


/*
looks for a node in linked list of FrameNode struct
by name, if name is found, gets the specific node out of list and returns 
it back. other wise it returns NULL.
Input: FrameNode** first, char* name.
Output: FrameNode* element\NULL.
*/
FrameNode* popFrame(FrameNode** first, char* name)
{
	bool check = false;
	FrameNode* curr = *first;
	FrameNode* removedFrame = NULL;
	if (!strcmp(curr->frame->name, name))//if the first name is our name.
	{
		removedFrame = curr;
		*first = curr->next;
		removedFrame->next = NULL;
	}
	else
	{
		while (!check && curr->next)
		{
			if (!strcmp(name, curr->next->frame->name))//if the name is found - but it's not the first.
			{
				removedFrame = curr->next;
				curr->next = curr->next->next;
				removedFrame->next = NULL;
				check = true;
			}
			else
			{
				curr = curr->next;
			}
		}
	}
	return removedFrame;
}


/*
Counts how many nodes there are in a FrameNode list.
Input: FrameNode* curr.
Output: FrameNode* list length.
*/
int listLength(FrameNode* curr)
{
	int ans = 0;
	if (curr)
	{
		ans = 1 + listLength(curr->next);
	}
	return ans + 1;
}

/*
Adds a frame to a list. Updates first node if list is empty.
Input: FrameNode** first, FrameNode* frame.
Output : none.
*/

void addToList(FrameNode** first, FrameNode* frame)
{
	FrameNode* current = *first;
	FrameNode* temp = NULL;
	bool frameAdded = false;
	if (*first)
	{
//Does it until frame's added.	
		while (!frameAdded)
		{
			if (!current->next)
			{
				current->next = frame;
				frameAdded = true;
			}
			current = current->next;
		}
	}
	else //in case frame is empty.
	{
		*first = frame;
	}
}

/*
Gets str and limit to get from user and end	with '\n'.
Input: char str[], int n.
Output: none.
*/
void myFgets(char str[], int n)
{
	fgets(str, n, stdin);
	str[strcspn(str, "\n")] = 0;
}


/*
Dynamically allocates a FrameNode struct.
Input: char framePath[], char frameName[], int frameTime
Output: created node.
*/
FrameNode* createFrame(char framePath[], char frameName[], int frameTime)
{
	Frame* tempFrame = (Frame*)malloc(sizeof(Frame));
	FrameNode* tempFrameNode = (FrameNode*)malloc(sizeof(FrameNode));
	tempFrame->name = (char*)malloc(strlen(frameName) + 1);
	tempFrame->path = (char*)malloc(strlen(framePath) + 1);
	
	strcpy(tempFrame->path, framePath);
	strcpy(tempFrame->name, frameName);
	
	tempFrame->duration = frameTime;
	tempFrameNode->frame = tempFrame;
	tempFrameNode->next = NULL;
	return tempFrameNode;
}


/*
Prints all elements (frames) of the linked list.
Input: FrameNode** first.
Output: true\false. (1\0)
*/
void printElementList(FrameNode* first)
{
	FrameNode* curr = first;
	printf("\t\tName\tDuration\tPath\n");
	while (curr)
	{
		printf("\t\t%s\t%u ms\t\t%s\n", curr->frame->name, curr->frame->duration, curr->frame->path);
		curr = curr->next;
	}
}

/*
changes duration of a specific frame according to input.
Input: FrameNode** first
Output: true\false. (1\0)
*/
bool removeFromList(FrameNode** first, char name[])
{
	FrameNode* temp = NULL;
	FrameNode* current = *first;
	bool frameRemoved = false;
	if (*first && !strcmp((*first)->frame->name, name))
	{
		temp = (*first)->next;
		free(*first);
		*first = temp;
		frameRemoved = true;
	}
	else
	{
		while (current && !frameRemoved) //checks 
		{
			if (current->next && !strcmp(current->next->frame->name, name))
			{
				temp = current->next->next;
				free(current->next);
				current->next = temp;
				frameRemoved = true;
			}
			current = current->next;
		}
	}
	return frameRemoved;
}

/*
changes duration of a specific frame according to input.
Input: FrameNode** first.
Output: none.
*/
void changeAllDurations(FrameNode** first, int newDuration)
{
	FrameNode* curr = *first;
	while (curr)
	{
		curr->frame->duration = newDuration;
		curr = curr->next;
	}
}

/*
changes duration of a specific frame according to input.
Input: FrameNode** first.
Output: true\false. (1\0).
*/
bool changeDuration(FrameNode** first, char* frameName)
{
	FrameNode* curr = *first;
	int newDuration = 0;

	printf("Enter duration to change:\n");
	scanf("%d", &newDuration);
	getchar();
	
	//if there's any value in there:
	while (curr)
	{
		if (!strcmp(curr->frame->name, frameName))
		{
			curr->frame->duration = newDuration;
			return 1;
		}
		curr = curr->next;
	}
	return 0;
}

/*
opens a txt file that user put in 
gets info from it.
Input: FrameNode* first.
Output: true/false.
*/
bool openProject(FrameNode** first)
{
	char moviePath[MAX_LEN]; //sFname
	char temp[MAX_LEN];

	char frameName[MAX_LEN];
	unsigned int frameDuration = 0;
	char framePath[MAX_LEN];
	int len = 0;

	char* tok;
	FrameNode* curr = *first;
	FrameNode* newFrame = NULL;
	int count = 0;

	printf("Please insert your movie file <with full path>:\n");
	myFgets(moviePath, MAX_LEN);
	FILE* handler = fopen(moviePath, "r");
	if (!handler)
	{
		printf("Failed to open project %s.\n", moviePath);
		return false;
	}
	else
	{
		while (fgets(temp, MAX_LEN, handler))
		{		
			tok = strtok(temp, DELIMETER);
			count = 0;
			while (tok != 0)
			{
				count++;
				switch (count)
				{
					case 1:
						strcpy(frameName, tok);
						break;
					case 2:
						 frameDuration = atoi(tok);
						break;
					case 3:
						len = strlen(tok);
						strcpy(framePath, tok);
						framePath[len - 1] = '\0';
						break;
					default:
						printf("Illigal token.\n");
				}
				tok = strtok(0, DELIMETER);
			}
			newFrame = createFrame(framePath, frameName, frameDuration);
			addToList(first, newFrame);
		}
		printf("Project loaded successfully!\n");
	}
	fclose(handler);
	return true;
}


/*
saves project to a txt file using a the
delimeter variable to seperate each piece of info
Input: FrameNode* first.
Output: none.
*/
void saveProject(FrameNode* first)
{
	char moviePath[MAX_LEN];
	FrameNode* curr = first;
	
	printf("Please insert movie file <with full path>:\n");
	myFgets(moviePath, MAX_LEN);
	FILE* handler = fopen(moviePath, "w");
	if (!handler)
	{
		printf("Not a valid path.\n");
	}
	else
	{
		while (curr)
		{
			fprintf(handler ,"%s#%u#%s\n", curr->frame->name, curr->frame->duration, curr->frame->path);
			curr = curr->next;
		}
		printf("movie was saved!\n");
	}
	fclose(handler);
}

/*
reverses all nodes in list.
Input: FrameNode** first.
Output: none.
*/
void reverse(FrameNode** first)
{
	FrameNode* current = *first;
	FrameNode* prev = NULL;
	FrameNode* next = NULL;
	while (current)
	{
		next = current->next;
		current->next = prev;
		prev = current;
		current = next;
	}
	*first = prev;
}


/*
frees all allocates memory of linked list.
Input: FrameNode** first.
Output: none.
*/
void freelist(FrameNode** first)
{
	FrameNode* curr = *first;
	while (curr)
	{
		if (curr->frame)
		{
			free(curr->frame->path);
			free(curr->frame->name);
			free(curr->frame);
		}
		curr = curr->next;
	}
	*first = NULL;
}
