/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:                          *
* Credits:                       *
**********************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <dirent.h>
#include <crtdbg.h>

//function stamps
char* readFile(char* filename, int* size);
int checkFile(char* fname, char* signature, int signatureSize, FILE* pLogFile, int scanMode);
bool greeting(char** argv);
void add2Log(FILE* pLogFile, char* str);

#define MAX_SIZE 300
#define FALSE 0
#define MAX_ARGS 3
#define EURISTIC_FACTOR_QUICK 0.20
#define EURISTIC_FACTOR_FULL 0.5

//enums for index mapping 
enum SCAN_MODES
{
	QUICK = 1,
	FULL = 2
};


enum INDEXES
{
	FOLDER_PATH = 1,
	STAMP_NAME = 2
};


int main(int argc, char** argv)
{
//checks if amount of arguments is valid.	
	if (argc != MAX_ARGS)
	{
		printf("ERROR! usage: <folder to scan> <file of virus stamp>.\n");
		return 1;
	}
	int choice = 0;
	int signatureSize = 0;
	int scanMode = 0;
	struct dirent* dir = 0;
	char* FilesDirPath = argv[FOLDER_PATH];
	char* VirusStampFileName = argv[STAMP_NAME];
	char sLogFileName[MAX_SIZE];
	char fullName[MAX_SIZE];
	DIR* filesDir = opendir(FilesDirPath);
	FILE* pLogFile = NULL;


//checks if folder path is valid.
	if (filesDir == NULL)
	{
		printf("%s is not a valid folder.\n", FilesDirPath);
		return 1;
	}

//creates path to open log afterwards. 
	sprintf(sLogFileName, "%s/%s", FilesDirPath, "AntiVirusLog.txt");
	pLogFile = fopen(sLogFileName, "wt");
	if (!pLogFile)
	{
		printf("Failed to open log file.\n");
		return 1;
	}
//adds defualt messges to log.
	add2Log(pLogFile, "Anti - virus began! Welcome!\n\nFolder to scan: \n");
	add2Log(pLogFile, FilesDirPath);
	add2Log(pLogFile, "\n");

//gets signature from user and checks for NULL in case it didn't work.
	char* signature = readFile(VirusStampFileName, &signatureSize);
	if (!signature)
	{
		printf("%s id not a valid file.\n", signature);
		return 1;
	}
//adds defualt messges to log.
	add2Log(pLogFile, "Virus signature: \n");
	add2Log(pLogFile, VirusStampFileName);
	add2Log(pLogFile, "\n\n");

//gets scan choice from user.
	choice = greeting(argv);
//sets scanning mode flag.
	add2Log(pLogFile, "Scanning option:\n");
	if (choice == FALSE)
	{
		scanMode = FULL;
		add2Log(pLogFile, "Normal Scan\n\n");
	}
	else
	{
		scanMode = QUICK;
		add2Log(pLogFile, "Quick Scan\n\n");
	}
	add2Log(pLogFile, "Results:\n");
	printf("This process may take several minutes...\n\n");

//uses dir path to send each file path to scaning function.
	while ((dir = readdir(filesDir)) != NULL)
	{
		if (strcmp(dir->d_name, ".") &&
			strcmp(dir->d_name, "..") &&
			dir->d_type != DT_DIR)
//sets fullName as full path to file and sends it to checkFile().
		{
			sprintf(fullName,"%s/%s",FilesDirPath ,dir->d_name);
			checkFile(fullName, signature, signatureSize, pLogFile, scanMode);
		}
	}
//adds defualt messges to log.
	printf("Scan Completed.\n");
	printf("See log for results: %s", sLogFileName);
//closes files.	
	fclose(pLogFile);
	closedir(filesDir);
	getchar();
	getchar();
	return 0;
}

//looks better in main(). (less parametres as well)
void add2Log(FILE* pLogFile, char* str)
{
	fwrite(str, sizeof(char), strlen(str), pLogFile);
}

/*
input: friends, num
function will print greeting
and returns choice
output: choice
*/
bool greeting(char** argv)
{
	int choice = 0;
	
	printf("Welcome to my virus Scan!\n\n");
	printf("Folder to scan: %s\n", argv[FOLDER_PATH]);
	printf("Folder signature: %s\n\n", argv[STAMP_NAME]);
	printf("Press 0 for a normal scan or any other key for a quick scan: ");
	scanf("%d", &choice);
	return choice;
}

/*
input: fname, signature, signatureSize, pLogFile, scanMode
function will scan the elected buffer each time and
compare it to signature string and print to user/log
the resualt accordingly.
output: int
*/
int checkFile(char* fname, char* signature, int signatureSize, FILE* pLogFile, int scanMode)
{
	int i = 0;
	int res = 0;
	long int fSize = 0;
	int read_size;
	double factor = 0.0; //long float
	char* buffer = NULL;
	char str[MAX_SIZE];

//sets buffer at size signature.
	buffer = (char*)malloc(sizeof(char) * (signatureSize + 1));

//if malloc memory wasn't allocated properly.
	if (!buffer)
	{
		printf("Error in malloc for buffer.\n");
		return 1;
	}

//opens file to scan from.
	FILE* handler = fopen(fname, "rb");
	if (!handler) 
	{
		printf("Error in opening input file %s\n", fname);
		free(buffer);
		return 2;
	}

//checkss for factor according to scanMode flag.
	if (scanMode == QUICK)
	{
		factor = EURISTIC_FACTOR_QUICK;
	}
	else
	{
		factor = EURISTIC_FACTOR_FULL;
	}
//determine the file size: 	
	fseek(handler, 0L, SEEK_END);
	fSize = ftell(handler);

	fseek(handler, 0, SEEK_SET);

//scan start in while that breaks in diffrent returns.
	while (TRUE)
	{
		if ((i > fSize * factor) && (i < fSize * (1 - factor)))
		{
			i++;
			continue;
		}
//moves handler in file by onr byte.
		res = fseek(handler, i++, SEEK_SET);
		if (res == 3)
		{
			printf("End of file %s reached\n", fname);
			return 3;
		}

//gets a chunck of the file to read and compare to signature size.
		read_size = fread(buffer, sizeof(char), signatureSize, handler);
//clean case.
		if (signatureSize != read_size)
		{
			sprintf(str, "%s - Clean\n", fname);
			printf("%s", str);
			add2Log(pLogFile, str);
			free(buffer);
			fclose(handler);
			return 4;
		}

		buffer[signatureSize] = '\0';

//if the signature wasn't found - memcmp resault > 0.
		if (memcmp(buffer, signature, signatureSize))
			continue;

//if it is zero, that means that signature was found.
		else
		{
//decides scan according to scanMode Flag using enum.
			if (scanMode == QUICK)
			{
				if (i < (fSize / 2))
				{
					sprintf(str, "%s - Infected! (first %2.f%%)\n", fname, factor*100);
				}
				else
				{
					sprintf(str, "%s - Infected! (last %2.f%%)\n", fname, factor*100);
				}
			}
			else
			{
				sprintf(str, "%s - Infected!\n", fname);
			}
//adds to log file.
			printf("%s", str);
			add2Log(pLogFile, str);
			free(buffer);
			fclose(handler);
			return 5;
		}

	}
	fclose(handler);
	free(buffer);
	return 0;
}

/*
input:  filename,  size
function will read the signature file and return
signature to user
output: buffer
*/
char* readFile(char *filename, int *size)
{
	char* buffer = NULL;
	int string_size, read_size;
	FILE* handler = fopen(filename, "rb");

//if signature file was open sucsessfuly.
	if (handler)
	{
		fseek(handler, 0, SEEK_END);
		string_size = ftell(handler);
		rewind(handler); 

//allocates memory to buffer (signature).
		buffer = (char*)malloc(sizeof(char) * (string_size + 1));
//reads signature.
		read_size = fread(buffer, sizeof(char), string_size, handler);
		buffer[string_size] = '\0';

		if (string_size != read_size)
		{
			free(buffer);
			buffer = NULL;
		}
		*size = read_size;
		fclose(handler);
	}
//if file was not opened.
	else
		printf("Error opening %s file.\n ", filename);

	return buffer;
}