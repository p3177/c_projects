/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:                          *
* Credits:                       *
**********************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <dirent.h>
#include <crtdbg.h>


char* readFile(char* filename, int* size);
int checkFile(char* fname, char* signature, int signatureSize, FILE* pLogFile, int scanMode);
bool greeting(char** argv);
void add2Log(FILE* pLogFile, char* str);
int checkFileAmount(dirent* dir, DIR* filesDir);

#define MAX_SIZE 300
#define FALSE 0
#define MAX_ARGS 3
#define EURISTIC_FACTOR_QUICK 0.2
#define EURISTIC_FACTOR_FULL 0.5
#define _CRTDBG_MAP_ALLOC

enum SCAN_MODES
{
	QUICK = 1,
	FULL = 2
};


enum INDEXES
{
	FOLDER_PATH = 1,
	STAMP_NAME = 2
};


int main(int argc, char** argv)
{
	if (argc != MAX_ARGS)
	{
		printf("ERROR! usage: <folder to scan> <file of virus stamp>.\n");
		return 1;
	}
	int choice = 0;
	int signatureSize = 0;
	int scanMode = 0;
	int fileAmount = 0;
	struct dirent* dir = 0;
	char* FilesDirPath = argv[FOLDER_PATH];
	char* VirusStampFileName = argv[STAMP_NAME];
	char sLogFileName[MAX_SIZE];
	char fullName[MAX_SIZE];
	DIR* filesDir = opendir(FilesDirPath);
	FILE* pLogFile = NULL;



	if (filesDir == NULL)
	{
		printf("%s is not a valid folder.\n", FilesDirPath);
		return 1;
	}

	sprintf(sLogFileName, "%s/%s", FilesDirPath, "AntiVirusLog.txt");
	pLogFile = fopen(sLogFileName, "wt");
	if (!pLogFile)
	{
		printf("Failed to open log file.\n");
		return 1;
	}
	add2Log(pLogFile, "Anti - virus began! Welcome!\n\nFolder to scan: \n");
	add2Log(pLogFile, FilesDirPath);
	add2Log(pLogFile, "\n");

	char* signature = readFile(VirusStampFileName, &signatureSize);
	if (!signature)
	{
		printf("%s id not a valid file.\n", signature);
		return 1;
	}
	add2Log(pLogFile, "Virus signature: \n");
	add2Log(pLogFile, VirusStampFileName);
	add2Log(pLogFile, "\n\n");

	choice = greeting(argv);
	add2Log(pLogFile, "Scanning option:\n");
	if (choice == FALSE)
	{
		scanMode = FULL;
		add2Log(pLogFile, "Normal Scan\n\n");
	}
	else
	{
		scanMode = QUICK;
		add2Log(pLogFile, "Quick Scan\n\n");
	}
	add2Log(pLogFile, "Results:\n");
	printf("This process may take several minutes...\n\n");
	fileAmount = checkFileAmount(dir, filesDir);
	printf("amount of files: %d", fileAmount);


	printf("Scan Completed.\n");
	printf("See log for results: %s", sLogFileName);
	fclose(pLogFile);
	closedir(filesDir);
	getchar();
	getchar();
	return 0;
}

int checkFileAmount(dirent* dir, DIR* filesDir)
{
	int count = 0;
	while ((dir = readdir(filesDir)) != NULL)
	{
		if (strcmp(dir->d_name, ".") &&
			strcmp(dir->d_name, "..") &&
			dir->d_type != DT_DIR)
		{
			count++;
		}
	}
	return count;
}

void add2Log(FILE* pLogFile, char* str)
{
	fwrite(str, sizeof(char), strlen(str), pLogFile);
}

bool greeting(char** argv)
{
	int choice = 0;
	
	printf("Welcome to my virus Scan!\n\n");
	printf("Folder to scan: %s\n", argv[FOLDER_PATH]);
	printf("Folder signature: %s\n\n", argv[STAMP_NAME]);
	printf("Press 0 for a normal scan or any other key for a quick scan: ");
	scanf("%d", &choice);
	return choice;
}


int checkFile(char* fname, char* signature, int signatureSize, FILE* pLogFile, int scanMode)
{
	int i = 0;
	int res = 0;
	long int fSize = 0;
	int read_size;
	double factor = 0.0; //long float
	char* buffer = NULL;
	char str[MAX_SIZE];

	buffer = (char*)malloc(sizeof(char) * (signatureSize + 1));

	if (!buffer)
	{
		printf("Error in malloc for buffer.\n");
		return 1;
	}

	FILE* handler = fopen(fname, "rb");
	if (!handler) 
	{
		printf("Error in opening input file %s\n", fname);
		free(buffer);
		return 2;
	}

	if (scanMode == QUICK)
	{
		factor = EURISTIC_FACTOR_QUICK;
	}
	else
	{
		factor = EURISTIC_FACTOR_FULL;
	}
//determine the file size: 	
	fseek(handler, 0L, SEEK_END);
	fSize = ftell(handler);

	fseek(handler, 0, SEEK_SET);

	while (TRUE)
	{
		if ((i > fSize * factor) && (i < fSize * (1 - factor)))
		{
			i++;
			continue;
		}
		res = fseek(handler, i++, SEEK_SET);
		if (res == 3)
		{
			printf("End of file %s reached\n", fname);
			return 3;
		}

		read_size = fread(buffer, sizeof(char), signatureSize, handler);
		if (signatureSize != read_size)
		{
			sprintf(str, "%s - Clean\n", fname);
			printf("%s", str);
			add2Log(pLogFile, str);
			free(buffer);
			fclose(handler);
			return 4;
		}

		buffer[signatureSize] = '\0';

		if (memcmp(buffer, signature, signatureSize))
			continue;

		else
		{
			if (scanMode == QUICK)
			{
				if (i < (fSize / 2))
				{
					sprintf(str, "%s - Infected! (first %2.f%%)\n", fname, factor*100);
				}
				else
				{
					sprintf(str, "%s - Infected! (last %2.f%%)\n", fname, factor*100);
				}
			}
			else
			{
				sprintf(str, "%s - Infected!\n", fname);
			}
			printf("%s", str);
			add2Log(pLogFile, str);
			free(buffer);
			fclose(handler);
			return 5;
		}

	}
	fclose(handler);
	free(buffer);
	return 0;
}

char* readFile(char *filename, int *size)
{
	char* buffer = NULL;
	int string_size, read_size;
	FILE* handler = fopen(filename, "rb");

	if (handler)
	{
		fseek(handler, 0, SEEK_END);
		string_size = ftell(handler);
		rewind(handler); 

		buffer = (char*)malloc(sizeof(char) * (string_size + 1));
		read_size = fread(buffer, sizeof(char), string_size, handler);
		buffer[string_size] = '\0';

		if (string_size != read_size)
		{
			free(buffer);
			buffer = NULL;
		}
		*size = read_size;
		fclose(handler);
	}
	else
		printf("Error opening %s file.\n ", filename);

	return buffer;
}