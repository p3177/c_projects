/***********
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:                          *
* Credits:                       *
************/

#include <stdio.h>
#include <dirent.h>
#include <stdbool.h>
#define STR_LEN 150
bool isinfected(char* file_buffer, char* sign_buffer, int file_len, int sign_len);
bool quickscan(FILE* open_file, FILE* sign_read, int signlen, FILE* logfile);
bool normalScan(char* sign_read, int filelen, int signlen, FILE* open_file);



int main(int argc, char** argv)
{
    //argv[0] - program name
    //argv[1] - dir to look through
    //argv[2] - virus sign
    FILE* log_file = 0;

    bool is_infected = false, is_quick_infected = false;

    int filelen = 0, signlen = 0, user_choice = 0;


    char* file_read = "", * sign_read = "";

    char file_full_path[STR_LEN] = { 0 };
    char log_full_path[STR_LEN] = { 0 };

    DIR* d = 0;
    struct dirent* dir = 0;
    d = opendir(argv[1]);

    FILE* open_file = 0;

    FILE* sign = fopen(argv[2], "rb");
    fseek(sign, 0, SEEK_END);
    signlen = ftell(sign);//get sign len
    fseek(sign, 0, SEEK_SET);
    sign_read = (char*)malloc(sizeof(char) * signlen + 1);
    fread(sign_read, 1, signlen, sign);
    fclose(sign); // close sign file



    printf("Welcome to virus scanner\n1 - Normal scan\n2 - Quick scan\n3 - Exit\nEnter your choice:");
    scanf("%d", &user_choice);
    printf("Scanning:\n");
    if (d == NULL)
    {
        printf("Error opening directory");
        return 1;
    }
    while ((dir = readdir(d)) != NULL)
    {
        strcpy(log_full_path, argv[1]);
        strcat(log_full_path, "/");
        strcat(log_full_path, "VirusLog.txt");

        log_file = fopen(log_full_path, "w");
        fprintf(log_file, "Anti - virus began!Welcome!\n\n\Folder to scan:\n%s\nVirus signature:\n%s\n\nScanning option:\n", argv[1], argv[2]);


        if (strcmp(dir->d_name, ".") && strcmp(dir->d_name, "..") && dir->d_type != DT_DIR && strcmp(dir->d_name, "VirusLog.txt"))
        {


            strcpy(file_full_path, argv[1]);
            strcat(file_full_path, "/");
            strcat(file_full_path, dir->d_name); // get whole file path
            open_file = fopen(file_full_path, "rb");
            printf("%s - ", file_full_path);

            fseek(open_file, 0, SEEK_END);
            filelen = ftell(open_file);
            if (user_choice == 1)
            {
                fprintf(log_file, "Normal Scan\n\nResults:\n");
                is_infected = normalScan(sign_read, filelen, signlen, open_file);
                if (is_infected)
                {
                    printf("Infected!\n");
                    fprintf(log_file, "%s Infected!\n", file_full_path);
                }
                else
                {
                    printf("Clear\n");
                    fprintf(log_file, "%s Clear\n", file_full_path);

                }

            }
            else if (user_choice == 2)
            {
                fprintf(log_file, "Quick Scan\n\nResults:\n");
                is_quick_infected = quickscan(open_file, sign_read, signlen, log_file);
                if (!(is_quick_infected)) // if quick doesnt detect infection
                {
                    is_infected = normalScan(sign_read, filelen, signlen, open_file); //go to normal scanning

                    if (is_infected)
                    {
                        fprintf(log_file, "%s\n", file_full_path);
                        //infected(there's already a print in function)
                    }
                    else
                    {
                        printf("Clear\n");
                    }
                }
            }
            fclose(open_file); // close file
        }
    }
    fclose(log_file);
    free(sign_read); // free sign malloc


}


bool isinfected(char* file_buffer, char* sign_buffer, int file_len, int sign_len)
{
    //fil - file after malloc (file_buffer)
    // vir - sign after malloc (sign_buffer)
    int i = 0;


    for (i = 0; i < file_len - sign_len; ++i)
    {
        if (memcmp(sign_buffer, file_buffer + i, sign_len) == 0)
        {
            //infected
            return true;   // vir exists in fil found
        }
    }
    //clean
    return false;  // vir is not in file

}


bool quickscan(FILE* open_file, FILE* sign_read, int signlen, FILE* logfile)
{
    char* file_read;
    bool is_infected = false;
    int filelen = 0;

    filelen = (int)(ftell(open_file) * 0.2); // get 20 precent

    file_read = (char*)malloc(sizeof(char) * filelen + 1);
    fseek(open_file, 0, SEEK_SET);
    fread(file_read, 1, filelen, open_file);
    is_infected = isinfected(file_read, sign_read, filelen, signlen); // sends 20 %
    if (!(is_infected)) // is first 20% not infected
    {

        //last 20%

        fseek(open_file, 0, SEEK_END);
        fseek(open_file, -(filelen), SEEK_CUR);


        file_read = (char*)malloc(sizeof(char) * filelen + 1);
        fread(file_read, 1, filelen, open_file);

        is_infected = isinfected(file_read, sign_read, filelen, signlen);
        if (is_infected)
        {
            printf("Infected! (last 20%)\n");
            fprintf(logfile, "Infected! (last 20 %)\n");
            return true;
        }
        else
        {
            return false;

        }
    }
    else
    {
        printf("Infected! (first 20%)\n");
        fprintf(logfile, "Infected! (first 20%)\n");
        return true;
    }
    free(file_read); // free file malloc
}

bool normalScan(char* sign_read, int filelen, int signlen, FILE* open_file)
{
    char* file_read = "";
    bool is_infected;
    file_read = (char*)malloc(sizeof(char) * filelen + 1);
    fseek(open_file, 0, SEEK_SET);
    fread(file_read, 1, filelen, open_file);
    fclose(open_file); // close file
    is_infected = isinfected(file_read, sign_read, filelen, signlen);
    if (is_infected)
    {
        return true;
    }
    else
    {
        return false;
    }
    free(file_read);
}